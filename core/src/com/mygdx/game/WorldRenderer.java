package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.math.Vector2;

public class WorldRenderer {
	private MyGame myGame;
	private World world;
	private Texture charactorImg1;
	private Texture charactorImg2;
	private SpriteBatch batch;
	public static final int BLOCK_SIZE = 40;
	private MapRenderer mapRenderer;
	private BitmapFont font;
	private Player1 player1;
	private Player2 player2;
	
	public WorldRenderer(MyGame myGame , World world){
		font = new BitmapFont();
		this.myGame = myGame;
		batch = myGame.batch;
		this.world = world;
		player1 = world.getPlayer1();
		player2 = world.getPlayer2();
		mapRenderer = new MapRenderer(myGame.batch ,world.getMap(), world);
		charactorImg1 = new Texture("Dog.png");
		charactorImg2 = new Texture("Smurf.png");
	}
	public void render (float delta){
		mapRenderer.render();
		Vector2 pos = world.getPlayer1().getPosition();
		Vector2 pos2 = world.getPlayer2().getPosition();
        batch.begin();
        batch.draw(charactorImg1, pos.x - BLOCK_SIZE/2, MyGame.HEIGHT - pos.y - BLOCK_SIZE/2);
        batch.draw(charactorImg2, pos2.x - BLOCK_SIZE/2, MyGame.HEIGHT - pos2.y - BLOCK_SIZE/2);
        font.draw(batch, "player1 :" + player1.score, 160, 40);
        font.draw(batch, "player2 :" + player2.score, 560, 40);
        font.draw(batch, "Time :" + world.countTime, 360, 580);
        if(World.endGame) {
        	font.draw(batch, "Game Over !", 340, 440);
        	font.draw(batch, "ENTER TO RESTART !", 310, 400);
        	if(World.player1Win) {
        		font.draw(batch, "Player 1 Win !", 360, 400);
        	}else if(World.player2Win) {
        		font.draw(batch, "Player 2 Win !", 360, 400);
        	} 
        }
        batch.end();
	}
}
