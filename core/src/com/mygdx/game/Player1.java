package com.mygdx.game;

import java.security.SecureRandom;
import java.util.Random;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Player1 {
	private Texture charactorImg1;
	private MyGame myGame;
	private Player2 player2;
	private World world;
	private Map map;
	public boolean player1DirectionUp;
	public boolean player1DirectionDown;
	public boolean player1DirectionLeft;
	public boolean player1DirectionRight;
    public static final int DIRECTION_UP = 1;
    public static final int DIRECTION_RIGHT = 2;
    public static final int DIRECTION_DOWN = 3;
    public static final int DIRECTION_LEFT = 4;
    public static final int DIRECTION_STILL = 0;
    private static final int [][] DIR_OFFSETS = new int [][] {
        {0,0},
        {0,-1},
        {1,0},
        {0,1},
        {-1,0}
    };
    public static final int SPEED = 5;
    private int currentDirection;
    private int nextDirection; 
    Vector2 position;
    public static int score = 0;
	private int canDigTime = 10000; 
	private int digTime = 0 ; 
	private char [] item  = new char [] {'b','b','b','b','b','b','b','b','b','b','b','b','b','c','c',
										 'c','c','c','c','d','d','d','d','d','d','d','d','r','r','r',
										 'r','r','h','h','h','h','j','i','i','d','d','d','d','d','d',
										 'a','a','a','a','a','a','a','a','m','m','m','m','m','m','m',
										 'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
										 'c','c','c','c','c','c','c','c','c','c','c','c','c','c','c',
										 'd','d','d','d','d','d','d','d',' ',' ',' ',' ',' ',' ',' ',
										 ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',
										 ' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' ',' '} ;
	/* b = bomb
	 * c = carrot 
	 * m = coin 
	 * a = apple
	 * j = blue jewery
	 * r = red crystal 
	 * d = shovel (dig) 
	 */
	private char [] itemBag = new char [10];
	private int bagItem;
	private static Random RANDOM = new SecureRandom();
	
    public Player1(int x, int y,World world,Map map) {
    	position = new Vector2(x,y);
    	currentDirection = DIRECTION_STILL;
    	nextDirection = DIRECTION_STILL;
    	this.world = world;
    	this.map = map ;
    	this.player2 = world.getPlayer2();
    }    
 
    public Vector2 getPosition() {
        return position;    
    }
    
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        update(delta);
        SpriteBatch batch = myGame.batch;
        Vector2 pos = world.getPlayer1().getPosition();
        batch.begin();
        batch.draw(charactorImg1, pos.x - 20, MyGame.HEIGHT - pos.y - 20);
        batch.end();
    } 
    public void update(float delta){
    	Map map = world.getMap();
   	 	putItemToBasket();
    	if(isAtCenter()) {
            if(canMoveInDirection(nextDirection)) {
                currentDirection = nextDirection;   
            } 
            else {
                currentDirection = DIRECTION_STILL;    
            }
        }
   		position.x += SPEED * DIR_OFFSETS[currentDirection][0];
   		position.y += SPEED * DIR_OFFSETS[currentDirection][1];
		if(score<0) {
			score = 0;
		}
    }
    public void move(int dir) { 
        position.x += SPEED * DIR_OFFSETS[dir][0];
        position.y += SPEED * DIR_OFFSETS[dir][1];
    }
    public void setNextDirection(int dir) {
    	nextDirection = dir ; 
    }
    public boolean isAtCenter() {
        int blockSize = WorldRenderer.BLOCK_SIZE;
        return ((((int)position.x - blockSize/2) % blockSize) == 0) && ((((int)position.y - blockSize/2) % blockSize) == 0);
    }
    public boolean onFloor(){
    	return map.MAP[getRow()+1].charAt(getColumn()) == 'W';
    }
    private int getRow() {
        return ((int)position.y) / WorldRenderer.BLOCK_SIZE; 
    }
    private int getColumn() {
        return ((int)position.x) / WorldRenderer.BLOCK_SIZE; 
    }
    public boolean canMoveInDirection(int dir){
    	map = world.getMap();
    	int newRow = getRow() + DIR_OFFSETS[dir][1];
    	int newCol = getColumn() + DIR_OFFSETS[dir][0];
    	if (world.map.hasWallAt(newRow, newCol) || world.map.hasWoodBlockAt(newRow, newCol) || world.map.hasRockBlockAt(newRow, newCol))
    		return false ;
    	return true ; 
    }
    public void Dig(){
    	int x = getColumn();
    	int y = getRow();
    	if(canDig()){
    		if (player1DirectionDown){
    			if(map.MAP[y+1].charAt(x) == 'W'){
    				map.MAP[y+1].setCharAt(x, item[RANDOM.nextInt(item.length)]);
    			}
    		}
    		else if (player1DirectionLeft){
    			if(map.MAP[y].charAt(x-1) == 'W'){
    				map.MAP[y].setCharAt(x-1, item[RANDOM.nextInt(item.length)]);
    			}
    		}
    		else if(player1DirectionRight){
    			if(map.MAP[y].charAt(x+1) == 'W'){
    				map.MAP[y].setCharAt(x+1, item[RANDOM.nextInt(item.length)]);
    			}
    		}
    		digTime++;
    	}
    	//printMap();
    }
    
    public boolean canDig(){
    	return digTime <= canDigTime ; 
    }
    
	public void printMap(){
		for ( int i = 0 ; i < map.MAP.length ; i++){
			System.out.println(map.MAP[i]);
		}
	}
	/* b = bomb
	 * c = carrot 
	 * m = coin 
	 * a = apple
	 * j = blue jewery
	 * d = shovel (dig) 
	 */
	public void getItem(int x ,int y){
		if(canGetItem()){
			if(map.MAP[y].charAt(x) == 'b'){
				map.MAP[y].setCharAt(x, '.');
				score -= 100;
				bombRun(x,y);
			}
			else if(map.MAP[y].charAt(x) == 'c'){
				putItemInBag(map.MAP[y].charAt(x));
				map.MAP[y].setCharAt(x, '.');
			}	
			else if(map.MAP[y].charAt(x) == 'j'){
				putItemInBag(map.MAP[y].charAt(x));
				map.MAP[y].setCharAt(x, '.');
			}
			else if(map.MAP[y].charAt(x) == 'd'){
				for(int i = 0 ; i<8 ; i++){
					if(map.MAP[y].charAt(i+11) == 'W'){
						map.MAP[y].setCharAt(11+i, '.');
					}
				}
				map.MAP[y].setCharAt(x,'.');
			}
			else if(map.MAP[y].charAt(x) == 'a'){
				putItemInBag(map.MAP[y].charAt(x));
				map.MAP[y].setCharAt(x, '.');
			}
			else if(map.MAP[y].charAt(x) == 'm'){
				putItemInBag(map.MAP[y].charAt(x));
				map.MAP[y].setCharAt(x, '.');
			}	
		}
	}
	
	private void putItemInBag(char item){
		itemBag[bagItem] = item ;
		bagItem++;
	}
	private void putItemToBasket(){
		if(map.MAP[getRow()].charAt(getColumn()) == 'I'){
			for(int i = 0 ; i<bagItem ; i++){
				checkItem(itemBag[i]);
			}
			bagItem = 0;
		}
	}
	private void checkItem(char item){
		if(item == 'c'){
			score += 50;
		}
		else if(item == 'm'){
			score += 500;
			Player2.score -= 500;
		}
		else if(item == 'j'){
			score += 2000;
		}
		else if(item == 'a'){
			score += 100;
		}
	}
	private boolean canGetItem(){
		return bagItem < itemBag.length ;
	}
	private void bombRun(int x,int y){
		if(map.MAP[y+1].charAt(x) == 'W'){
			map.MAP[y+1].setCharAt(x,'.');
		}
		if(map.MAP[y+1].charAt(x+1) == 'W'){
			map.MAP[y+1].setCharAt(x,'.');
		}
		if(map.MAP[y+1].charAt(x-1) == 'W'){
			map.MAP[y+1].setCharAt(x,'.');
		}
		if(map.MAP[y].charAt(x+1) == 'W'){
			map.MAP[y].setCharAt(x,'.');
		}
		if(map.MAP[y].charAt(x-1) == 'W'){
			map.MAP[y].setCharAt(x,'.');
		}
		if(map.MAP[y-1].charAt(x) == 'W'){
			map.MAP[y-1].setCharAt(x,'.');
		}
		if(map.MAP[y-1].charAt(x+1) == 'W'){
			map.MAP[y-1].setCharAt(x+1,'.');
		}
		if(map.MAP[y-1].charAt(x-1) == 'W'){
			map.MAP[y-1].setCharAt(x-1,'.');
		}
	}
	public void movefollowMap(){
		if(getRow()>1) {
			position.y = position.y-40;
		}
	}
	public void pickItem(){
		 getItem(getColumn(),getRow());
	}
}
    
