package com.mygdx.game;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class MapRenderer {
	private Map map;
	private SpriteBatch batch;
	private Texture wallImg;
	private Texture woodBlockImg;
	private Texture rockBlockImg;
	private Texture bombImg;
	private Texture carrotImg;
	private Texture coinImg;
	private Texture shovelImg;
	private Texture appleImg;
	private Texture blueCrystalImg;
	private Texture basketImg;
	private World world;
	
	public MapRenderer(SpriteBatch batch,Map map, World world){
		this.map = world.getMap();
		this.batch = batch;
		this.world = world;
		wallImg = new Texture("wall.png");
		woodBlockImg = new Texture("WoodBlock.png");
		rockBlockImg = new Texture("RockBlock.png");
		bombImg = new Texture("bomb.png");
		carrotImg = new Texture("carrot.png");
		coinImg = new Texture("coin.png");
		shovelImg = new Texture("shovel.png");
		appleImg = new Texture("apple.png");
		blueCrystalImg = new Texture("jewelry.png");
		basketImg = new Texture("basket.png");
	}
	
	public void render(){
		batch.begin();
		for( int r = 0 ; r < world.map.getHeight() ; r++ ){
			for( int c = 0 ; c < world.map.getWidth() ; c++){
				int x = c*WorldRenderer.BLOCK_SIZE;
				int y = MyGame.HEIGHT - ( r * WorldRenderer.BLOCK_SIZE) - WorldRenderer.BLOCK_SIZE;
				
				if(world.map.hasWoodBlockAt(r, c)){
					batch.draw(woodBlockImg, x,y);
				} else if (world.map.hasRockBlockAt(r, c)){
					batch.draw(rockBlockImg, x, y );
				} else if (world.map.hasWallAt(r, c)){
					batch.draw(wallImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'b'){
					batch.draw(bombImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'd'){
					batch.draw(shovelImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'a'){
					batch.draw(appleImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'm'){
					batch.draw(coinImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'j'){
					batch.draw(blueCrystalImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'c'){
					batch.draw(carrotImg, x,y);
				}else if(map.MAP[r].charAt(c) == 'I'){
					batch.draw(basketImg, x,y);
				}
			}
		}
		batch.end();
	}

}
