package com.mygdx.game;

import java.util.concurrent.TimeUnit;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.utils.TimeUtils;

public class World {
	private Player1 player1;
	private Player2 player2;
	private MyGame myGame;
	public static boolean player1Win;
	public static boolean player2Win;
	public static boolean endGame;
	public Map map;
	public long startTime = TimeUtils.millis();
	public long currentTime ;
	public long countTime;
	public long gameTime = 125*1000; //(seconds*1000)
	
	World (MyGame myGame) {
		map = new Map();
		this.myGame = myGame;
		player1 = new Player1(60,60,this,map);
		player2 = new Player2(500,60,this,map);
	}
	
	public void update(float delta) {
		currentTime = TimeUtils.timeSinceMillis(startTime);
		countTime = gameTime-currentTime;
		countTime = TimeUnit.MILLISECONDS.toSeconds(countTime);
		if(countTime > 0) {
			player1.update(delta);
			player2.update(delta);
			map.mapMove(currentTime,this);
		} else {
			countTime = 0;
			map.clearMap() ;
			endGame = true;
			if(Gdx.input.isKeyJustPressed(Keys.ENTER)) {
				startTime = TimeUtils.millis();
				currentTime = TimeUtils.timeSinceMillis(startTime);
				countTime = gameTime-currentTime;
				countTime = TimeUnit.MILLISECONDS.toSeconds(countTime);
				player1.position.x = 60;
				player1.position.y = 60;
				player2.position.x = 500;
				player2.position.y = 60;
				map.MapReset();
				endGame = false;
			}
		}
	}
	
	Player1 getPlayer1() {
		return player1;
	}
	Player2 getPlayer2() {
		return player2;
	}
	
	Map getMap() {
		return map;
	}	
	
	public void checkScore(){
		if(Player1.score > Player2.score) {
			player1Win = true;
		} else if(Player2.score > Player1.score) {
			player2Win = true;
		};
	}
}
