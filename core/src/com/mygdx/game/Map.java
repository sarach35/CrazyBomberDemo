package com.mygdx.game;

import java.security.SecureRandom;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Map {
	public StringBuilder[] MAP;
	private StringBuilder newMapLine;
	private StringBuilder topMapLine;
	private int height;
	private int width; 
	private boolean [][] hasWoodBlock ;
	private World world;
	private long changeTime = 2000;
	private Player1 player1;
	private Player2 player2;
	private static Random RANDOM = new SecureRandom();
	
	public Map(){	
		MAP = new StringBuilder[15];
		MAP[0] = new StringBuilder("####################");
		MAP[1] = new StringBuilder("#I.......RR.......I#");
        MAP[2] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[3] = new StringBuilder("#WWRWWWWWRRWWWWWRWW#");
        MAP[4] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[5] = new StringBuilder("#WWWWRWWWRRWWWRWWWW#");
        MAP[6] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[7] = new StringBuilder("#WRWRWRWWRRWWRWRWRW#");
        MAP[8] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[9] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[10] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[11] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[12] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[13] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[14] = new StringBuilder("####################");
        
		height = MAP.length;
		width = MAP[0].length();
		initWoodBlockData();
	}
	
	public int getHeight() {
		return height;
	}
	
	public int getWidth(){
		return width;
	}
	
	public boolean hasWoodBlockAt ( int r , int c){
		return MAP[r].charAt(c) == 'W' ;
	}
	
	public boolean hasRockBlockAt ( int r , int c){
		return MAP[r].charAt(c) == 'R';
	}
	
	public boolean hasWallAt( int r , int c){
		return MAP[r].charAt(c) == '#';
	}
	
	private void initWoodBlockData() {
		hasWoodBlock = new boolean[height][width];
		for(int r = 0 ; r < height ; r++ ){
			for(int c = 0 ; c < width ; c++ ) {
				hasWoodBlock[r][c] = MAP[r].charAt(c) == 'W';
			}
		}
	}
	
	public void mapMove(long time,World world){
		topMapLine = new StringBuilder("#I.......RR.......I#");
		if(time >= changeTime){
			for(int i = 1 ; i<MAP.length-2 ; i++) {
				MAP[i] = MAP[i+1];
			}
			generateMap();
			MAP[MAP.length-2] = newMapLine;
			MAP[1] = topMapLine;
			changeTime += 1500;
			world.getPlayer1().movefollowMap();
			world.getPlayer2().movefollowMap();
		}
	}
	public void generateMap(){
		String [] newLine = new String [] {"#WWWWWWWWRRWWWWWWWW#",
										   "#WWWWWWWWRRWWWWWWWW#",
										   "#WWWWWWWWRRWWWWWWWW#",
				                           "#WWWWWWWWRRWWWWWWWW#",
										   "#WWRWWWRWRRWRWWWRWW#",
										   "#WWWWWWRWRRWRWWWRWW#",
										   "#WRWRWRWWRRWWRWRWRW#",
										   "#WWRWWWRWRRWRWWWRWW#",
										   "#WRRWRRRWRRWRRWRRRW#",
										   "#WWWRWWWWRRWWWWRWWW#",
										   "#WWWRWWWWRRWWWWRWWW#",
										   "#RRWWWWWWRRWWWWWWRR#",
										   "#RRWWWWWWRRWWWWWWRR#",
										   "#RRWWWWWWRRWWWWWWRR#",
										   "#RRWWWWWWRRWWWWWWRR#",
									       "#WRWRWRWRRRRWRWRWRW#"};
		newMapLine = new StringBuilder(newLine[RANDOM.nextInt(newLine.length)]);
	}
	
	public void clearMap() {
		for( int y = 0 ; y < getHeight() ; y++ ){
			for( int x = 0 ; x < getWidth() ; x++){
					MAP[y].setCharAt(x, '#');
			}
		}
	}
	
	public void MapReset() {
		MAP[0] = new StringBuilder("####################");
		MAP[1] = new StringBuilder("#I.......RR.......I#");
        MAP[2] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[3] = new StringBuilder("#WWRWWWWWRRWWWWWRWW#");
        MAP[4] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[5] = new StringBuilder("#WWWWRWWWRRWWWRWWWW#");
        MAP[6] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[7] = new StringBuilder("#WRWRWRWWRRWWRWRWRW#");
        MAP[8] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[9] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[10] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[11] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[12] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[13] = new StringBuilder("#WWWWWWWWRRWWWWWWWW#");
        MAP[14] = new StringBuilder("####################");
	}
}
