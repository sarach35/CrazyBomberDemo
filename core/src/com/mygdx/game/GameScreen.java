package com.mygdx.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class GameScreen extends ScreenAdapter {
	private MyGame myGame;
	private Texture charactorImg1;	
	private Player1 player1;
	private World world;
	private WorldRenderer worldRenderer;
	
	public GameScreen(MyGame myGame){
		this.myGame = myGame;
		charactorImg1 = new Texture("Dog.png");
		world = new World(myGame);
		worldRenderer = new WorldRenderer(myGame,world);		
	}
	public void render(float delta){
    	update(delta);
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        worldRenderer.render(delta);
	}
	public void update(float delta){
		updatePlayer1_Direction();
		updatePlayer1();
		updatePlayer2_Direction();
		updatePlayer2();
		world.update(delta);
	}
	public void updatePlayer1_Direction(){
    	Player1 player1 = world.getPlayer1();
        if(Gdx.input.isKeyPressed(Keys.W)) {
        	player1.setNextDirection(Player1.DIRECTION_UP);
        	player1.player1DirectionUp = true;
        	player1.player1DirectionDown = false;
        	player1.player1DirectionLeft = false;
        	player1.player1DirectionRight = false;
        }
        else if(Gdx.input.isKeyPressed(Keys.S)) {
        	player1.setNextDirection(Player1.DIRECTION_DOWN);
         	player1.player1DirectionUp = false;
        	player1.player1DirectionDown = true;
        	player1.player1DirectionLeft = false;
        	player1.player1DirectionRight = false;
        } 
        else if(Gdx.input.isKeyPressed(Keys.A)) {
        	player1.setNextDirection(Player1.DIRECTION_LEFT);
         	player1.player1DirectionUp = false;
        	player1.player1DirectionDown = false;
        	player1.player1DirectionLeft = true;
        	player1.player1DirectionRight = false;
        } 
        else if(Gdx.input.isKeyPressed(Keys.D)) {
        	player1.setNextDirection(Player1.DIRECTION_RIGHT);
         	player1.player1DirectionUp = false;
        	player1.player1DirectionDown = false;
        	player1.player1DirectionLeft = false;
        	player1.player1DirectionRight = true;       
        }
        else {
        	player1.setNextDirection(Player1.DIRECTION_STILL);
        }
    }
	public void updatePlayer1(){
		Player1 player1 = world.getPlayer1();
		if(Gdx.input.isKeyJustPressed(Keys.Y)){
			player1.Dig();
		} else if(Gdx.input.isKeyJustPressed(Keys.T)) {
			player1.pickItem();
		}
	}
	public void updatePlayer2_Direction(){
    	Player2 player2 = world.getPlayer2();
        if(Gdx.input.isKeyPressed(Keys.UP)) {
        	player2.setNextDirection(Player1.DIRECTION_UP);
        	player2.player2DirectionUp = true;
        	player2.player2DirectionDown = false;
        	player2.player2DirectionLeft = false;
        	player2.player2DirectionRight = false;
        }
        else if(Gdx.input.isKeyPressed(Keys.DOWN)) {
        	player2.setNextDirection(Player1.DIRECTION_DOWN);
         	player2.player2DirectionUp = false;
        	player2.player2DirectionDown = true;
        	player2.player2DirectionLeft = false;
        	player2.player2DirectionRight = false;
        } 
        else if(Gdx.input.isKeyPressed(Keys.LEFT)) {
        	player2.setNextDirection(Player1.DIRECTION_LEFT);
         	player2.player2DirectionUp = false;
        	player2.player2DirectionDown = false;
        	player2.player2DirectionLeft = true;
        	player2.player2DirectionRight = false;
        } 
        else if(Gdx.input.isKeyPressed(Keys.RIGHT)) {
        	player2.setNextDirection(Player1.DIRECTION_RIGHT);
         	player2.player2DirectionUp = false;
        	player2.player2DirectionDown = false;
        	player2.player2DirectionLeft = false;
        	player2.player2DirectionRight = true;       
        }
        else {
        	player2.setNextDirection(Player1.DIRECTION_STILL);
        }
    }
	public void updatePlayer2(){
		Player2 player2 = world.getPlayer2();
		if(Gdx.input.isKeyJustPressed(Keys.O)){
			player2.Dig();
		} else if(Gdx.input.isKeyJustPressed(Keys.P)) {
			player2.pickItem();
		}
	}
}
